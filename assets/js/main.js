require.config({
    paths: {
        knockout: 'components/knockout.js/knockout',
        datepicker: 'components/pickadate/lib/picker.date',
        timepicker: 'components/pickadate/lib/picker.time',
        jquery: 'components/jquery/jquery',
        backbone: 'components/backbone/backbone',
        'jquery-validation': 'components/jquery-validation/jquery.validate',
        requirejs: 'components/requirejs/require',
        pickadate: 'components/pickadate/lib/picker',
        underscore: 'components/underscore/underscore'
    },
    shim: {
        underscore: {
            init: function () {
        return _.noConflict();
    },
            exports: '_'
        },
        datepicker: {
            deps: [
                'jquery',
                'components/pickadate/lib/picker'
            ],
            exports: 'jQuery.fn.pickadate'
        },
        timepicker: {
            deps: [
                'jquery',
                'components/pickadate/lib/picker'
            ],
            exports: 'jQuery.fn.pickatime'
        }
    }
});

define('underscore', [], function() {
    return window._;
});

define('backbone', [], function() {
    return window.Backbone;
});

// Use WordPress's jQuery library
define('jquery', [], function() {
    return window.jQuery;
});

// Use WordPress's Backbone library
//define('backbone', [], function () { return window.Backbone; });

var init = function($) {

//	var media = $('.a360-core-field-image');
//	if (media.length)
//	{
//		require(['media'], function(initMedia) {
//			initMedia(media);
//		});
//	}
};

define('main', ['jquery'], init);
define('compiled', ['jquery'], init);

// Compile all in r.js
if (typeof window === "undefined")
{
    require(['a360', 'field/date']);
}