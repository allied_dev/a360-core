
define('field/date', ['jquery', 'knockout', 'datepicker', 'timepicker'], function($, ko) {
	
	return function(el, model, data, defaultData) {

		el = $(el);
		
		model.data = ko.observableArray(data);
		model.addField = function() {
			model.data.push(defaultData);
		};
		model.removeField = function(index) {
			model.data.splice(index(), 1);
			if (model.data().length === 0)
			{
				model.addField();
			}
		};
		model.selectValue = function(select, value) {
			$(select).val(value);
		};
		ko.applyBindings(model, el[0]);

		var input = el.find('input'),
			container = el.closest('.postbox').find('.before-table'),
			hidden = $('<div></div>').css({
				position:'absolute',
				top:0,
				left:0,
				overflow:'hidden',
				width:'1px',
				height:'1px'
			}).insertAfter(input),
			time = $('<input type="text">').appendTo(hidden),
			date = $('<input type="text">').appendTo(hidden),
			timepicker = time.pickatime({
				container: container,
				onSet: function(item) {
					if ( 'select' in item ) setTimeout( function() {
						input
							.val( datepicker.get() + ' ' + timepicker.get() )
							.focus()
							.on('focus', datepicker.open);
					}, 0 );
				}
				
			}).pickatime('picker'),
			datepicker = date.pickadate({
				container: container,
				onSet: function(item) {
					if ( 'select' in item ) setTimeout( function() {
						input.off('focus').val( datepicker.get() );
						timepicker.open();
					}, 0 );
				},
				format: 'yyyy-mm-dd'
			}).pickadate('picker');

		input.on('focus', datepicker.open).on('click', function(e) {
			e.stopPropagation();
			datepicker.open();
		});

		// WordPress fix for pickadate
		$('.picker:not(.boundFix)').on('mousedown', function(event) {
			// * For mousedown events, cancel the default action in order to
			//   prevent cases where focus is shifted onto external elements
			//   when using things like jQuery mobile or MagnificPopup (ref: #249 & #120).
			if ( ! $( event.target ).is( ':input' ) ) {

				event.preventDefault();

				// Re-focus onto the element so that users can click away
				// from elements focused within the picker.
				//input.focus();
			}
		}).addClass('boundFix');

	};
});