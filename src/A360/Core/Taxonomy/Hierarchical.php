<?php

namespace A360\Core\Taxonomy;

use \A360\Core\Util;
use \A360\Core\Taxonomy;

class Hierarchical extends Taxonomy
{
	protected static function default_labels($labels = array())
	{
		$singular	= 'Category';
		$plural		= 'Categories';

		is_string($labels) and $labels = array('singular_name' => $labels);
		if (isset($labels['singular_name']))
		{
			$singular	= $labels['singular_name'];
			$plural		= isset($labels['name']) ? $labels['name'] : Util\Inflector::pluralize($singular);
		}
		elseif ( ! is_array($labels) or ! isset($labels['name']))
		{
			// Use WordPress defaults
			return array();
		}

		return array_merge(array(
			'name'              => _x( $plural, 'taxonomy general name' ),
			'singular_name'     => _x( $singular, 'taxonomy singular name' ),
			'search_items'      => __( 'Search '.$plural ),
			'all_items'         => __( 'All '.$plural ),
			'parent_item'       => __( 'Parent '.$singular ),
			'parent_item_colon' => __( 'Parent '.$singular.':' ),
			'edit_item'         => __( 'Edit '.$singular ),
			'update_item'       => __( 'Update '.$singular ),
			'add_new_item'      => __( 'Add New '.$singular ),
			'new_item_name'     => __( 'New '.$singular.' Name' ),
			'menu_name'         => __( $plural ),
		), $labels);
	}

	protected static function default_args($args = array())
	{
		return array_merge(array(
			'hierarchical' => true
		), $args);
	}
}