<?php
/**
 * Created by PhpStorm.
 * User: StevenKohlmyer
 * Date: 2/13/14
 * Time: 3:12 PM
 */


namespace A360\Core;

abstract class Rewrite
{
	/**
	 * This is a helper function to test for a slug (string) inside of the current url
	 *
	 * @param string	$slug This is the slug to search the url for;
	 * @param string	$callback This is the callback function to run when slug is in url.
	 * @param boolean	$starts_with This decides whether to explicitly check at the beginning of the request string for the match.
	 * @param int		$status This is the status to return to the client when the slug is matched.
	 */
	public function __construct($slug = null, $callback = null, $starts_with = false, $status = 200) {

		if(is_null($slug)) return;

		add_action('template_redirect', function($slug, $callback, $starts_with = false, $status = 200) {

			self::rewrite($slug, $callback, $starts_with, $status);

		});

	}

	/**
	 * This is a helper function to test for a slug (string) inside of the current url
	 *
	 * @param string	$slug This is the slug to search the url for;
	 * @param string	$callback This is the callback function to run when slug is in url.
	 * @param boolean	$starts_with This decides whether to explicitly check at the beginning of the request string for the match.
	 * @param int		$status This is the status to return to the client when the slug is matched.
	 */
	public static function rewrite($slug, $callback, $starts_with = false, $status = 200)
	{

		global $wp;

		if (!($requestString = (string) $wp->request) or empty($slug)) {

			return false;

		}

		if ($starts_with) {

			$matches = (strpos($requestString, $slug) === 0);

		} else {


			$matches = (strpos($requestString, $slug) !== false);

		}

		if ($matches) {

			status_header($status);

			$callback();

		}
	}

	/**
	 * This is a helper function that locates and renders a template file.
	 *
	 * This method searches for the template file in the following locations (in order): child theme, parent theme.
	 *
	 * @param string	$fileName The file name of the template file to render.
	 */
	public static function template($fileName)
	{

		// Append .php file extension if missing
		if(strpos($fileName) === false) {
			$fileName .= '.php';
		}

		if (file_exists(STYLESHEETPATH . '/' . $fileName)) {

			// Child theme path file
			$return_template = STYLESHEETPATH . '/' . $fileName;

		} elseif (file_exists(TEMPLATEPATH . '/' . $fileName)) {

			// Parent Theme Path File
			$return_template = TEMPLATEPATH . '/' . $fileName;

		} else {

			// Failed finding given template file in child & parent theme directories.
			return;

		}

		include($return_template);

		die();

	}
}