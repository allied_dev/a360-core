<?php

namespace A360\Core;

interface iShortcode
{
	public static function render($atts);
}

abstract class Shortcode implements iShortcode
{
    CONST SHORTCODE = '';
    
	public static function register($shortcode = null)
	{
		$class		= '\\'.ltrim(get_called_class(), '\\');
		$defaults	= isset(static::$defaults) ? static::$defaults : null;
        
        if (empty($shortcode) && static::SHORTCODE) {
            $shortcode = static::SHORTCODE;
        }
        
        if (empty($shortcode)) {
            return;
        }
        
        $callback = function($atts, $content = null) use($class, $defaults) {

			$defaults and $atts = shortcode_atts($defaults, $atts);

			ob_start();
			$content = call_user_func($class.'::render', $atts, $content);
			$content and print $content;
			return ob_get_clean();
		};
        
        foreach ((array)$shortcode as $code) {
            add_shortcode($code, $callback);
        }
        
	}
}