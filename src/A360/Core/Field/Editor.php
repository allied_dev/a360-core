<?php

namespace A360\Core\Field;

use A360\Core;
use A360\Core\Field;
use A360\Core\FieldGroup;

class Editor extends Field
{
	protected $settings = array();

	public function __construct($name, $title, FieldGroup $field_group = null, $settings = array())
	{
		parent::__construct($name, $title, $field_group);

		$this->settings = array_merge(array(
			'media_buttons'	=> false,
			'teeny'			=> false,
		), $settings);
	}

	public function editor($value)
	{
    	global $typenow, $post;
    	switch ($typenow ?: $post->post_type) {
			case 'listing':
				$prefix = Core::BGPREFIX;
				break;
			default:
				$prefix = Core::PREFIX;
		}
		$settings = array_merge($this->settings, array(
			'textarea_name' => $this->get_field_name()
		));
		?>
		<tr class="<?= $this->get_ui_class(); ?>">
			<td colspan="2">
				<label><?= $this->title; ?></label><br />
				 <?php wp_editor(html_entity_decode($value), uniqid('editor-'.$prefix.'-'), $settings); ?>
			</td>
		</tr>
		<?php
	}
}