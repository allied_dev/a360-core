<?php

namespace A360\Core\Field;

use \A360\Core\Field;

class Url extends Field
{

	public static function render_field_template()
	{
		?>
		<input type="text" class="regular-text" placeholder="http://" data-bind="attr: { name: $root.field, value: $data }">
		<a href="#" data-bind="visible: $root.multiple, click: function() { $root.removeField($index) }">Clear</a>
		<?php
	}
}