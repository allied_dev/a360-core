<?php

namespace A360\Core\Field;

use \A360\Core\Util;

class Address extends Serialized
{
	public static $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
	public static $states = array(
		'AL' => "Alabama",
		'AK' => "Alaska",
		'AZ' => "Arizona",
		'AR' => "Arkansas",
		'CA' => "California",
		'CO' => "Colorado",
		'CT' => "Connecticut",
		'DE' => "Delaware",
		'DC' => "District Of Columbia",
		'FL' => "Florida",
		'GA' => "Georgia",
		'HI' => "Hawaii",
		'ID' => "Idaho",
		'IL' => "Illinois",
		'IN' => "Indiana",
		'IA' => "Iowa",
		'KS' => "Kansas",
		'KY' => "Kentucky",
		'LA' => "Louisiana",
		'ME' => "Maine",
		'MD' => "Maryland",
		'MA' => "Massachusetts",
		'MI' => "Michigan",
		'MN' => "Minnesota",
		'MS' => "Mississippi",
		'MO' => "Missouri",
		'MT' => "Montana",
		'NE' => "Nebraska",
		'NV' => "Nevada",
		'NH' => "New Hampshire",
		'NJ' => "New Jersey",
		'NM' => "New Mexico",
		'NY' => "New York",
		'NC' => "North Carolina",
		'ND' => "North Dakota",
		'OH' => "Ohio",
		'OK' => "Oklahoma",
		'OR' => "Oregon",
		'PA' => "Pennsylvania",
		'RI' => "Rhode Island",
		'SC' => "South Carolina",
		'SD' => "South Dakota",
		'TN' => "Tennessee",
		'TX' => "Texas",
		'UT' => "Utah",
		'VT' => "Vermont",
		'VA' => "Virginia",
		'WA' => "Washington",
		'WV' => "West Virginia",
		'WI' => "Wisconsin",
		'WY' => "Wyoming"
	);

	protected static $default = array(
		'street'	=> '',
		'street2'	=> '',
		'street3'	=> '',
		'locality'	=> '',
		'region'	=> '',
		'country'	=> 'United States',
		'postcode'	=> '',
        'phone'     => '',
        'label'     => '',
	);
	
	public static function render_field_template()
	{
		?>
		<div style="overflow:hidden">
			<div style="float:left;margin-right:20px;">
                <p data-bind="visible: $root.args.with_label">
					<input type="text" class="regular-text" placeholder="Location Name (optional)" data-bind="attr: { name: $root.field+'[label]', value: label }">
				</p>
				<p>
					<input type="text" class="regular-text" placeholder="Street" data-bind="attr: { name: $root.field+'[street]', value: street }">
				</p>
				<p data-bind="visible: $root.args.max_streets >= 2">
					<input type="text" class="regular-text" placeholder="Street 2" data-bind="attr: { name: $root.field+'[street2]', value: street2 }">
				</p>
				<p data-bind="visible: $root.args.max_streets >= 3">
					<input type="text" class="regular-text" placeholder="Street 3" data-bind="attr: { name: $root.field+'[street3]', value: street3 }">
				</p>
				<p>
					<input type="text" data-bind="attr: { name: $root.field+'[locality]', value: locality, placeholder: getCityLabel() }">
					<!-- ko ifnot: domestic -->
					<input type="text" data-bind="attr: { name: $root.field+'[region]', value: region, placeholder: getStateLabel() }" style="width:120px">
					<!-- /ko -->
					<!-- ko if: domestic -->
					<?= Util::form_select(array('data-bind' => 'attr: { name: $root.field+\'[region]\', afterRender: $root.selectValue($element, region, $index()) }'), array('' => ' - Choose State - ') + static::$states, ''); ?>
					<!-- /ko -->
				</p>
				<p>
					<?= Util::form_select(array(
							'data-bind' => 'visible: $root.args.international, attr: { name: $root.field+\'[country]\', afterRender: $root.selectValue($element, country(), $index()) }, event: { change: function() { $root.setCountry($element.value, $index()) } }',
							'style'		=> 'width:120px;'
						),
						array_combine(static::$countries, static::$countries),
						''
					); ?>
					<input type="text" data-bind="attr: { name: $root.field+'[postcode]', value: postcode, placeholder: getZipLabel() }">
				</p>
                <p data-bind="visible: $root.args.with_phone">
					<input type="text" class="regular-text" placeholder="Location Phone Number" data-bind="attr: { name: $root.field+'[phone]', value: phone }">
				</p>
			</div>
			<p>
				<a href="#" data-bind="visible: $root.multiple, click: function() { $root.removeField($index) }">Clear</a>
			</p>
		</div>
		<?php
	}
	
	protected $args = array(
		'max_streets'	=> 1,
		'international'	=> false,
        'with_phone' => false,
        'with_label' => false,
	);

	public function get_post_values()
	{
		$v = parent::get_post_values();
		
		if (empty($v) or ! $this->multiple)
		{
			return $v;
		}

		$values		= array();
		$current	= array();
		foreach ($v as $pair)
		{
			$current = array_merge($current, $pair);
			if (isset($pair['phone'])) // Needs to always be the last field!
			{
				$values[] = $current;
				$current = array();
			}
		}

		return $values;
	}
	
	public function deserialize_value($value = null)
	{
		if (is_array($value))
		{
			foreach ($value as &$v)
			{
				$v = $this->deserialize_value($v);
			}
			return $value;
		}
		
		return $value ? (object)array_merge(static::$default, (array)json_decode($value)) : (object)static::get_default_value();
	}
	
	public function queue_viewmodel($element_id, $data)
	{
		// Knockout view model definition
		$data		= json_encode($data);
		$default	= json_encode(static::$default);
		$model		= $this->get_viewmodel($element_id);

		add_action('admin_footer', function() use($model, $element_id, $data, $default) {
			?>
			<script>
			a360Ready(['field/address'], function(bind) {
				bind('#<?= $element_id; ?>', <?= $model; ?>, <?= $data; ?>, <?= $default; ?>);
			});
			</script>
			<?php
		});
	}

}
