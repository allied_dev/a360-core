<?php
namespace A360\Core;

use WP_User;

class User extends Base
{
	protected static $instances = array();

	public static function forge($id, $role = null)
	{
		if (isset(static::$instances[$id]))
		{
			return static::$instances[$id];
		}

		if ( ! ($user = new WP_User($id)) or ($role and ! in_array($role, $user->roles)))
		{
			return false;
		}

		return new static($user);
	}
	
	protected $user;
	
	public function __construct(WP_User $user)
	{
		$this->user = $user;
	}
	
	public function __get($name)
	{
		return $this->user->$name;
	}
	
	public function __isset($name)
	{
		return isset($this->user->$name);
	}
	
	public function __set($name, $value)
	{
		$this->user->__set($name, $value);
	}
}