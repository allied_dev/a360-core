<?php

namespace A360\Core;

use A360\Core;

class FieldGroup extends Base
{
	protected static $attr_reader = array('name', 'title', 'content_type', 'fields');

	public static function enqueue_assets()
	{
		$screen = \get_current_screen();
		if ($screen->post_type === 'page')
		{
			return;
		}

		$base = Core::get_core_public_uri();
		$require = array(
			'urlArgs' => 'v='.Core::ASSET_VERSION,
			'config' => array(
				'a360' => array(
					'ajax_url' => admin_url( 'admin-ajax.php' )
				)
			)
		);
//		$file = empty($_GET['debug']) ? 'compiled' : 'main';
		$file = 'main';
		?>
		<script>
        var loadcb = [];
		var require = <?= json_encode($require); ?>;
		//var a360Ready = function(deps, cb) { require(['<?= $file; ?>'], function() { require(deps, cb); }); };
        var a360Ready = function(deps, cb) {
            loadcb.push([deps, cb]);
        };
		</script>
		<?php
        add_action('admin_print_footer_scripts', function() use($base, $file) {
            ?>
            <script src="<?= $base; ?>assets/js/require.min.js" data-main="<?= $base; ?>assets/js/<?= $file; ?>"></script>
            <script>
            require(['<?= $file; ?>'], function() {
                while (loadcb.length) {
                    var cb = loadcb.shift();
                    require(cb[0], cb[1]);
                    console.log(cb);
                }
            });
            </script>
            <?php
        }, 9999);
	}

	protected $content_type;
	protected $fields;
	protected $name;
	protected $title;
	protected $visible = true;

	public function __construct($name, $title, ContentType $content_type = null)
	{
		$this->name		= $name;
		$this->title	= $title;

		if ($content_type)
		{
			$this->content_type =& $content_type;
			$this->content_type->add_field_group($this);
		}

		if (is_admin())
		{
			add_action('add_meta_boxes', array($this, 'register'));
			add_action('admin_head', __CLASS__.'::enqueue_assets');

            add_action('admin_enqueue_scripts', function() {
                wp_enqueue_script('backbone');
            });
		}
	}

	public function add_field(Field $field)
	{
		$this->fields[$field->name] = $field;
	}

	public function get_fields()
	{
		return $this->fields;
	}

	public function register()
	{
		add_meta_box(Core::PREFIX.'-fg-'.$this->name, $this->title, array($this, 'render'), $this->content_type->name, 'normal', 'high');
	}

	public function get_prefix()
	{
		global $typenow;
		switch ($this->content_type->name) {
			case 'listing':
				$prefix = Core::BGPREFIX;
				break;
			default:
				$prefix = Core::PREFIX;
		}
		$prefix .= "_" . $this->content_type->name . "_";
        return $prefix;
	}

	/**
	 * @deprecated
	 *
	 * @param type $post_meta
	 * @param type $field
	 * @param type $use_default
	 * @return type
	 */
	public function get_field_value($post_meta, $field, $use_default = true)
	{
		return $field->get_value($post_meta, $use_default);
	}

	public function render($post)
	{
		if ( ! $this->visible())
		{
			return;
		}

		$meta	= get_post_custom($post->ID);
		$prefix	= $this->get_prefix();

		wp_nonce_field($prefix.'save', $prefix.'nonce');

		do_action($prefix.'before');
		?>
		<div class="before-table" style="position:relative;"></div>
		<table class="form-table a360-field-group">
			<tbody>
			<?php
			foreach ($this->fields as $field) :
				if ($field->visible()) :
					$value = $this->get_field_value($meta, $field);
					$field->editor($value);
				endif;
			endforeach;
			?>
			</tbody>
		</table>
		<?php
		do_action($prefix.'after');
	}

	public function on($hook, $callback, $priority = 10, $accepted_args = 1)
	{
		add_action($this->get_prefix().$hook, $callback, $priority, $accepted_args);

		return $this;
	}

	public function save($post_id)
	{
		// Check nonce on a per-meta-box basis
		$prefix = $this->get_prefix();
		if ( ! isset($_POST[$prefix.'nonce']) || ! wp_verify_nonce($_POST[$prefix.'nonce'], $prefix.'save'))
		{
			return false;
		}

		do_action($prefix.'beforeSave', $post_id);

		// Save each field
		foreach ($this->fields as $field)
		{
			$field->save($post_id);
		}

		do_action($prefix.'save', $post_id);
	}

}