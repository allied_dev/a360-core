<?php

namespace A360\Core;

class ContentType extends Base
{
	protected static $instances = array();
	protected static $attr_reader = array('name', 'types', 'field_groups', 'methods', 'labels', 'args');

	protected static $core_types = array(
		'post',
		'page',
		'attachment',
		'revision',
		'nav_menu_item'
	);

    public static $skip_save_hook = false;

	public static function instance($type)
	{
		if (isset(static::$instances[$type]))
		{
			return static::$instances[$type];
		}
		elseif (in_array($type, static::$core_types))
		{
			$instance = new CoreContentType($type);
			static::$instances[$type] =& $instance;
			return $instance;
		}
		else
		{
			return null;
		}
	}

	public static function action_save_post($post_id)
	{
		// Bail if we're doing an auto save
		if (defined('DOING_AUTOSAVE') and DOING_AUTOSAVE) return $post_id;

		// Make sure we're saving a recognized post type
		$post_type = get_post_type($post_id);
		if ( ! ($contenttype = static::instance($post_type)))
		{
			return;
		}

		// if our current user can't edit this post, bail
		// @todo Permissions / ACL?!
		//if ( ! current_user_can('edit_post')) return;

		// Save each field group
		foreach ($contenttype->field_groups as $group)
		{
			$group->save($post_id);
		}
	}

	public static function get_core($type)
	{
		return static::instance($type);
	}

	public static function posts()
	{
		return static::get_core('post');
	}


	protected $name;
	protected $title;
	protected $labels		= array();
	protected $args			= array();
	protected $field_groups = array();
	protected $taxonomies	= array();
	protected $methods		= array();

	public function __construct($name, $title = null, $labels = array(), $args = array())
	{
		// Make sure we haven't already defined this type
		if (isset(static::$instances[$name]))
		{
			throw new Exception('Content type "'.$name.'" already defined');
		}
		elseif (in_array($name, array('post', 'page', 'attachment', 'revision', 'nav_menu_item', 'action')))
		{
			throw new Exception('Cannot use reserved post type: '.$name);
		}

		// Set name and title
		$this->name		= $name;
		$this->title	= $title;

		// Provide shortcut helper that generates sensible defaults based on singular name
		is_scalar($labels) and $labels = array('singular_name' => $labels);
		if (isset($labels['singular_name']))
		{
			$labels = array_merge(array(
				'name'					=> $title,
				'add_new'				=> 'Add New '.$labels['singular_name'],
				'add_new_item'			=> sprintf('Add New %s', $labels['singular_name']),
				'edit_item'				=> sprintf('Edit %s', $labels['singular_name']),
				'new_item'				=> sprintf('New %s', $labels['singular_name']),
				'all_items'				=> sprintf('All %s', $title),
				'view_item'				=> sprintf('View %s', $labels['singular_name']),
				'search_items'			=> sprintf('Search %s', $title),
				'not_found'				=> sprintf('No %s found', $title),
				'not_found_in_trash'	=> sprintf('No %s found in Trash', $title),
				'parent_item_colon'		=> '',
				'menu_name'				=> $title,
			), $labels);
		}

		// Set labels
		$this->labels = $labels;

		// Arguments
		$this->args = array_merge(array(
			'public'				=> true,
			'publicly_queryable'	=> true,
			'show_ui'				=> true,
			'show_in_menu'			=> true,
			'query_var'				=> true,
			'has_archive'			=> true,
			'hierarchical'			=> false,
			'menu_position'			=> null,
			'rewrite'				=> true,
			'supports'				=> array('title') //false,
		), $args);

		//'rewrite'				=> array('slug' => $name),
		//'capability_type'		=> 'post',

		// Store in class instances
		static::$instances[$name] =& $this;

		// Register our hooks
		add_action('init', array($this, 'register'));
		add_action('save_post', __CLASS__.'::action_save_post');
	}

	public function add_label($labels, $value = null)
	{
		is_array($labels) or $labels = array($labels => $value);

		foreach ($labels as $label => $value)
		{
			if (is_null($value))
			{
				unset($this->labels[$label]);
			}
			else
			{
				$this->labels[$label] = $value;
			}
		}

		return $this;
	}

	public function add_arg($args, $value = null)
	{
		is_array($args) or $args = array($args => $value);

		foreach ($args as $arg => $value)
		{
			if (is_null($value))
			{
				unset($this->args[$arg]);
			}
			else
			{
				$this->args[$arg] = $value;
			}
		}

		return $this;
	}

	public function supports($capabilities = array())
	{
		is_array($capabilities) or $capabilities = array($capabilities);

		$this->args['supports'] = array_unique(array_merge($this->args['supports'], $capabilities));
	}

	public function add_taxonomy($name, $args = array())
	{
		$this->taxonomies[$name] = $args;

		return $this;
	}

	public function add_field_group(FieldGroup $group)
	{
		$this->field_groups[$group->name] =& $group;

		return $this;
	}

	public function add_method($name, \Closure $callback)
	{
		$this->methods[$name] = $callback;

		return $this;
	}

	public function get_fields()
	{
		$fields = array();
		foreach ($this->field_groups as $group)
		{
			foreach ($group->fields as $field)
			{
				$fields[$field->name] = $field;
			}
		}
		return $fields;
	}

	/**
	 * Function to register post type
	 */
	public function register()
	{
		$args = $this->args;
		empty($args['labels']) and $args['labels'] = $this->labels;

		register_post_type($this->name, $args);
	}

    public function on($hook, $callback, $priority = 10, $accepted_args = 1)
	{
		add_action($this->name.'.'.$hook, $callback, $priority, $accepted_args);

		return $this;
	}

    public function getName()
    {
        return $this->name;
    }

    public function getTitle()
    {
        return $this->title;
    }

}

class CoreContentType extends ContentType
{
	public function __construct($name, $title = null, $labels = array(), $args = array())
	{
		$this->name = $name;
	}

	public function register() {}
}